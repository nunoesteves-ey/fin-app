define(['views/details', 'services/intrinio-client'],
        function(detailsTemplate, companyDataClient) {

    var externals = {};

    externals.start = function() {
        renderView({ loading: true });

        companyDataClient.details()
            .then(renderView);
    }

    function renderView(environment) {
        detailsTemplate.render(environment);
    }
    
    return externals;
})