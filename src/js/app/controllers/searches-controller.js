define(['views/searches', 'services/intrinio-client'],
       function(searchTemplate, companyDataClient) {

    var internals = {};
    var externals = {};

    externals.start = function() {
        renderView();

        if (internals.lastSearch) {
            searchHandler(internals.lastSearch);
        }
    }

    function renderView(environment) {
        searchTemplate.render(environment);
        searchTemplate.bind('search', searchHandler);
        searchTemplate.bind('clickDetails', detailsHandler);
    }

    function searchHandler(searchTerm) {
        renderView({ searching: true });

        internals.lastSearch = searchTerm;

        companyDataClient.search(searchTerm, 10, 1)
            .then(renderView);
    }

    function detailsHandler(ticker) {
        companyDataClient.setTicker(ticker);
    }

    return externals;
})