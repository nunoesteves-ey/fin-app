define(function() {
    function CompanyDetails(data) {
        data = data || {};

        this.name = data.name;
        this.businessAddress = data.businessAddress;
        this.hqRegion = data.hqRegion;
        this.ceo = data.ceo;
        this.sectorInfo = data.sectorInfo;
        this.category = data.category;
        this.description = data.description;
    }

    CompanyDetails.prototype.instanceFromAPIResults = function(r) {
        return new CompanyDetails({
            name: r.name,
            businessAddress: r.business_address,
            hqRegion: r.hq_state + ', ' + r.hq_country,
            ceo: r.ceo,
            sectorInfo: r.sector + ', ' + r.industry_group,
            category: r.industry_category,
            description: r.long_description
        });
    }

    return CompanyDetails;
})