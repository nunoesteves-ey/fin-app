define(function() {
    function Company(ticker, name) {
        this.ticker = ticker;
        this.name = name;
    }

    Company.prototype.collectionFromAPIResults = function(searchResults) {
        return searchResults.data.map(function(companyData) {
            return new Company(companyData.ticker, companyData.name);
        });
    }

    return Company;
})