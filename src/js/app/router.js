define(function(require) {

    var externals = {};
    var internals = {};

    externals.start = function() {
        window.location.hash = window.location.hash || internals.routeMappings[defaultRoute].hash;

        handleRouting();
        setInterval(handleRouting, 60);
    }

    /**
     * Map specific window.location.hash values
     * to controllers
     */
    internals.routeMappings = {
        search: {
            hash: '#search',
            controller: 'controllers/searches-controller'
        },
        
        details: {
            hash: '#details',
            controller: 'controllers/details-controller'
        },

        financials: {
            hash: '#financials',
            controller: 'controllers/financials-controller'
        }
    }

    var defaultRoute = 'search';
    var currentHash = '';

    function getParams(windowHash) {
        return windowHash.split('?')[1];
    }

    function getHash(windowHash) {
        return windowHash.split('?')[0];
    }

    function getBrowserRoute(windowHash) {
        var foundRoute = Object.keys(internals.routeMappings).find(function(key) {
            return internals.routeMappings[key].hash === getHash(windowHash);
        });

        return foundRoute || defaultRoute;
    }

    function buildParams(urlParams) {
        if (!urlParams) {
            return;
        }

        return urlParams.split('&').reduce(function(acc, keyValue) {
                var pair = keyValue.split('=');
                
                acc[pair[0]] = pair[1];
                return acc;
            }, {});
    }

    function loadController(browserRoute, browserParams) {
        var params = buildParams(browserParams);

        require([internals.routeMappings[browserRoute].controller], function(controller) {
            controller.start(params);
        });
    }

    function handleRouting() {
        if (currentHash === window.location.hash) {
            return;
        }

        var browserRoute = getBrowserRoute(window.location.hash),
            browserParams = getParams(window.location.hash);
        
        var newHash = '#' + browserRoute + (browserParams ? '?' + browserParams : '');

        currentHash = window.location.hash = newHash;

        loadController(browserRoute, browserParams)
    }

    return externals;
});
