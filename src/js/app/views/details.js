define(['views/base'], function(base) {

    var internals = {};
    var externals = {};
    internals.elements = {};

    externals.render = function(environment) {
        base.renderHTML(base.menu('details'));

        if (environment.loading) {
            base.appendHTML(base.loader());
            return;
        }

        if (!environment.company) {
            base.appendHTML(noResults());
            return;
        }

        base.appendHTML(results(environment.company));
    }

    function noResults() {
        return base.wrap('<p>There are no data for the chosen ticker.</p>')
    }

    function results(company) {
        return base.wrap(
            '<div class="ui grid">' +
            '<div class="sixteen wide column left aligned"><h1>' + company.name + '</h1></div>' +
            '<div class="one wide column center aligned"><i class="marker icon"></i></div>' +
            '<div class="fifteen wide column left aligned text">' + 
            company.businessAddress + '<br>' + company.hqRegion + '</div>' +
            '<div class="one wide column center aligned"><i class="travel icon"></i></div>' +
            '<div class="fifteen wide column left aligned text">' + 
            company.ceo  + '</div>' +
            '<div class="one wide column center aligned"><i class="industry icon"></i></div>' +
            '<div class="fifteen wide column left aligned text">' + 
            company.sectorInfo + '<br>' + company.category + '</div>' +
            '<div class="sixteen wide column left aligned text"><p>' +
            company.description + '</p></div></div>'
        );
    }

    return externals;
});