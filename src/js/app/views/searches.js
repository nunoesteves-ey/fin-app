define(['views/base'], function(base) {

    var internals = {};
    var externals = {};
    internals.elements = {};

    externals.render = function(environment) {
        base.renderHTML(searchBox());

        if (!environment) {
            base.appendHTML(noSearch());
            return;
        }

        if (environment.searching) {
            base.appendHTML(base.loader());
            return;
        }

        if (environment.companies.length === 0) {
            base.appendHTML(emptyResults());
            return;
        }

        var body = environment.companies.map(searchResult).join(''),
            footer = tableFooter(environment);

        base.appendHTML(base.wrap(resultsTable(body, footer)));
    }

    externals.bind = function(event, handler) {
        var eventBinders = {
                search: searchBinder,
                clickDetails: clickBinder
            }
                
        return eventBinders[event](handler);
    }

    function searchBinder(handler) {
        $('#search').on('change', function(e) {
            var searchTerm = e.target.value.toLowerCase().split(' ').join('-');
            handler(searchTerm);
        });
    }

    function clickBinder(handler) {
        $('a[href="#details"]').on('click', function(e) {
            var ticker = e.target.id;
            handler(ticker);
        });
    }

    function searchBox() {
        return base.wrap(
            '<div class="ui large left icon input">' +
            '<i class="search icon"></i>' +
            '<input id="search" type="text" placeholder="Search for a company..." />' +
            '</div>'
        );
    }

    function noSearch() {
        return base.wrap(
            '<p>There are no results to show yet.</p>' +
            '<p>You can try searching for a ticker like APPL.</p>'
        );
    }

    function emptyResults() {
        return base.wrap('<p>There were no results.</p>');
    }

    function resultsTable(body, footer) {
        return base.wrap(
            '<table class="ui celled table">' +
            '<thead>' +
            '<th width="20%"><strong>Ticker</strong></th>' +
            '<th><strong>Company Name</strong></th>' +
            '</thead>' +
            '<tbody>' + body + '</tbody>' +
            '<tfoot>' + footer + '</tfoot>' +
            '</table></div>'
        );
    }

    function searchResult(company) {
        return '<tr>' +
               '<td><div class="ui ribbon label">' + company.ticker + '</div></td>' +
               '<td><a id="' + company.ticker + '" href="#details">' + 
               company.name + '</a></td>' +
               '</tr>';
    }

    function tableFooter(environment) {
        var currentPage = environment.page || 1;

        return '<tr><th colspan="3">' +
               '<div class="ui right floated pagination menu">' +
               '<a class="icon item" href="#search"><i class="left chevron icon"></i></a>' +
               '<a class="item disabled">...</a>' +
               '<a class="item" href="#search">' + (currentPage - 1) + '</a>' +
               '<a class="item active">' + (currentPage) + '</a>' +
               '<a class="item" href="#search">' + (currentPage + 1) + '</a>' +
               '<a class="item disabled">...</a>' +
               '<a class="icon item" href="#search"><i class="right chevron icon"></i></a>' +
               '</div></th><tr>';
    }

    return externals;
});