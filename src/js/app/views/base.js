define(function() {

    var internals = {};
    var externals = {};

    externals.renderHTML = function(content) {
        $('#app').html(content);
    }

    externals.appendHTML = function(content) {
        $('#app').append($(content));
    }

    externals.wrap = function(content) {
        return '<div class="ui center aligned container" style="margin-bottom: 1em;">' +
            content + '</div>';
    }

    externals.loader = function() {
        return this.wrap('<div class="ui huge active centered inline loader"></div>' +
            '<p>Loading</p>');
    }

    externals.menu = function(activeLink) {
        var detailsIsActive = 'details' === activeLink,
            financialsIsActive = 'financials' === activeLink;

        return this.wrap([
            '<div class="ui compact menu">',
            menuElement('Search Results', '#search', 'browser', false),
            menuElement('Company Details', '#details', 'newspaper', detailsIsActive),
            menuElement('Financials', '#financials', 'line chart', financialsIsActive),
            '</div>'
        ].join(''));
    }

    function menuElement(label, link, icon, isActive) {
        var linkClass = isActive ? 'item active' : 'item';

        return '<a class="' + linkClass + '" href="' + link + '">' +
               '<i class="' + icon + ' icon"></i>' +
               '<p>' + label + '</p>' +
               '</a>';
    }

    return externals;
});