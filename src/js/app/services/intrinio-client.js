define(['models/company', 'models/company-details', 'models/company-financials'],
        function(Company, CompanyDetails, CompanyFinancials) {
    
    var internals = {};
    var externals = {};

    internals.baseURL = 'https://api.intrinio.com';
    internals.username = '988536f00d211c69f1afe0310cc9bcc4';
    internals.password = '3dfe01a1eeb9e407c6f7c6aaa306c6ec';
    internals.auth = "Basic " + btoa(internals.username + ':' + internals.password)

    
    externals.search = function(searchTerm, pageSize, page, controllerCallback) {
        externals.lastSearch = searchTerm;

        var queryString = 'query=' + searchTerm +
                          '&page_size=' + (pageSize || 10) +
                          '&page_number=' + (page || 1);

        return apiRequest({ endpoint: 'companies', data: queryString })
            .then(parseSearchResponse);
    }

    externals.details = function(controllerCallback) {
        return apiRequest({ endpoint: 'companies', data: 'identifier=' + internals.ticker })
            .then(parseDetailResponse);
    }

    externals.setTicker = function(value) {
        internals.ticker = value;
    }

    // Response parsing
    function parseSearchResponse(response) {
        return {
            companies: Company.prototype.collectionFromAPIResults(response),
            page: response.current_page,
            totalPages: response.total_pages
        };
    }

    function parseDetailResponse(response) {
        var company = CompanyDetails.prototype.instanceFromAPIResults(response);

        return { company: company };
    }

    // Interaction with the Intrinio API
    function apiRequest(requestParams) {
        
        function successHandler(response) {
            return response.json();
        }

        function errorHandler(response) {
            return 'There was an error with the request: ' + response;
        }
        var url = internals.baseURL + '/' + requestParams.endpoint;

        var request = new Request(url + '?' + requestParams.data, {
            headers: {
                'Authorization': internals.auth,
                'Accept': 'application/json'
            },
            method: 'GET'
        });

        return fetch(request)
            .then(successHandler)
            .catch(errorHandler);
    }

    return externals;
});