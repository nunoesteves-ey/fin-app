# FinApp

Frontend for an API, showcases financial info about companies
that can be searched by their stock ticker and/or name.

Displays a search box where you can input company names. On change, returns
a list of tickers (paginated)

## Views

They receive the event handlers from the controllers and have a `render(params)`
method that receives the dynamic data within the `params` object.

* search: has a searchbox and lists tickers
* details: each company displays address, ceo, tags/activity, description
* statement: financial results for a given quarter

## Controllers

match the models

## Models

company, company details, company statement

## Data service

Fetches API data from [intrinio](https://api.intrinio.com), instantiates the 
necessary models and returns them to the controller. The main endpoint used is
`/companies`. The company provides extensive [API docs](http://docs.intrinio.com/).

## Router

makes use of the window.location.hash property
checks for changes on the property

## Code organization/dependencies

* maybe look at [requirejs](http://requirejs.org/) for modules and organization
* [html5 boilerplate](https://html5boilerplate.com/) for the project's file structure
* [semantic ui](https://semantic-ui.com/) for styling

## Flow

the entry point loads a main file
the main file loads a router file and calls router.start()

The router file defines an object that maps the window.location.hash value to a controller.
Start sets the initial hash, then calls setInterval(hashCheck, 100).

